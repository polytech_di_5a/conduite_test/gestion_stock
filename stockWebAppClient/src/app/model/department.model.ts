import { User } from './user.model';
import { Store } from './store.model';

export class Department {
	id: number;
  name: string;
  user: User;
  store: Store;
}