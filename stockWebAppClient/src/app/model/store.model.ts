import { User } from './user.model';

export class Store {
  id: number;
  name: string;
  user: User;
}