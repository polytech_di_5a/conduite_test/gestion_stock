import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from '../model/article.model';
import { Department } from '../model/department.model';
import { Store } from '../model/store.model';
import { User } from '../model/user.model';
import { AdminService } from '../services/admin.service';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  
  errorMessage: string;
  departmentList: Department[];
  storeList: Store[];
  articleList: Article[];
  userList: User[];

  constructor(private loginService: LoginService, private adminService: AdminService, private router: Router) { }

  ngOnInit() {
    //console.log(this);
    this.loginService.getAdminPage().subscribe(
      data => {
        this.getUsers();
        this.getDepartments();
        this.getArticles();
        this.getStores();
      },
      error => {
        this.errorMessage = `${error.status}: ${JSON.parse(error.error).message}`;
      }
    );
  }

  getUsers(): void {
    this.adminService.getUsers()
      .subscribe(userList => this.userList = userList);
  }

  getStores(): void {
    this.adminService.getStores()
      .subscribe(storeList => this.storeList = storeList);
  }

  addStore(name: string): void {
    name = name.trim();
    if(name.length>0){
      this.adminService.addStore({ name } as Store)
        .subscribe(store => { this.storeList.push(store); },
          error1 => {},
          () => {},
        );
     }
  }

  deleteStore(store: Store): void {
    let bool:Boolean=true;
    for(let department in this.departmentList){
      if(this.departmentList[department].store.id==store.id){
        bool=false;
        break;
      }
    }
    if(bool){
      this.storeList = this.storeList.filter(l => l !== store);
      this.adminService.deleteStore(store).subscribe();
    }
  }

  updateStoreContent(name: string, store: Store): void {
    if(name.length>0){
      store.name=name;
    }
    this.adminService.updateStore(store).subscribe();
  }

  updateStore(store: Store, user: User): void {
    if(store!==undefined&&user!==undefined){
      store.user = user;
      this.adminService.updateStore(store).subscribe();
      const index = this.userList.indexOf(user, 0);
      if (index > -1) {
         this.userList.splice(index, 1);
      }
    }
  }

  getDepartments(): void {
    this.adminService.getDepartments()
      .subscribe(departmentList => this.departmentList = departmentList);
  }

  addDepartment(name: string, store: Store): void {
    name = name.trim();
    if(name.length>0){
    this.adminService.addDepartment({ name, store } as Department)
      .subscribe(department => { this.departmentList.push(department); },
        error1 => {},
        () => {},
      );
    }
  }

  updateDepartmentContent(name: string, department: Department): void {
    if(name.length>0){
      department.name = name;
    }
    this.adminService.updateDepartment(department).subscribe();
  }

  updateDepartment(department: Department, user: User): void {
    if(department!==undefined&&user!==undefined){
      department.user = user;
      this.adminService.updateStore(department).subscribe();
      const index = this.userList.indexOf(user, 0);
      if (index > -1) {
         this.userList.splice(index, 1);
      }
    }
  }

  deleteDepartment(department: Department): void {
    let bool:Boolean=true;
    for(let article in this.articleList){
      if(this.articleList[article].department.id==department.id){
        bool=false;
        break;
      }
    }
    if(bool){
      this.departmentList = this.departmentList.filter(l => l !== department);
      this.adminService.deleteDepartment(department).subscribe();
    }
  }

  getArticles(): void {
    this.adminService.getArticles()
      .subscribe(articleList => this.articleList = articleList);
  }

  addArticle(name: string, stock: number, department: Department): void {
    if(name.length>0 && department!==undefined && !isNaN(stock) && stock>=0){
      this.adminService.addArticle({ name, stock, department } as Article)
        .subscribe(article => { 
          //console.log(article);
          this.articleList.push(article);},
          error1 => {},
          () => {},
        );
    }
  }

  updateArticle(name: string, article: Article): void {
    if(name.length>0){
      article.name = name;
    }
    this.adminService.updateArticle(article).subscribe();
  }

  updateArticleStock(stock: number, article: Article): void {
    if(!isNaN(stock) && stock>=0){
      article.stock = stock;
    }
    this.adminService.updateArticle(article).subscribe();
  }

  deleteArticle(article: Article): void {
    this.articleList = this.articleList.filter(l => l !== article);
    this.adminService.deleteArticle(article).subscribe();
  }

}


