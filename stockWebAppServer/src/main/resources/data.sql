INSERT INTO role (id, name) VALUES (1, 'ROLE_ADMIN') ON CONFLICT DO NOTHING;
INSERT INTO role (id, name) VALUES (2, 'ROLE_USER') ON CONFLICT DO NOTHING;
INSERT INTO users (id, username, password) VALUES (1, 'superadmin','$2a$10$dft2JK2J6fPrIIA4mdZ2aODalcAF.VfSffwcqUQLb1lMfN1DUGncS') ON CONFLICT DO NOTHING;
INSERT INTO users_roles (user_id, roles_id) VALUES (
	(Select id from users where username = 'superadmin'),
	(Select id from role where name = 'ROLE_ADMIN')
) ON CONFLICT DO NOTHING;