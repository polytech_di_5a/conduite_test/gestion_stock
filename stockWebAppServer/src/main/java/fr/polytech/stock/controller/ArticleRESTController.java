package fr.polytech.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.polytech.stock.model.Article;
import fr.polytech.stock.repository.ArticleRepository;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/restApi/articles")
public class ArticleRESTController {

    private ArticleRepository articleRepository;

    @Autowired
    public ArticleRESTController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    //get All
    @RequestMapping(method = RequestMethod.GET/*, produces = "application/xml"*/)
    //@GetMapping
    public List<Article> findAllArticles() {
        return articleRepository.findAll();
    }
    
    //get By Id
    @RequestMapping(value="/{id}", method = RequestMethod.GET/*, produces = "application/xml"*/)
    //@GetMapping
    public Article findArticle(@PathVariable("id") long id) {
        return articleRepository.findById(id);
    }
    
    //Create new entry
    @RequestMapping(method = RequestMethod.POST)
    //@PostMapping
    public ResponseEntity<Article> addArticle(@RequestBody Article article) {
        articleRepository.save(article);
        return new ResponseEntity<Article>(article, HttpStatus.CREATED);
    }

    //Delete By Id
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    //@DeleteMapping("/{id}")
    public ResponseEntity<Article> deleteArticle (@PathVariable("id") long id) {
        Article article = articleRepository.findById(id);
        if (article == null) {
            System.out.println("Article not found!");
            return new ResponseEntity<Article>(HttpStatus.NOT_FOUND);
        }

        articleRepository.deleteById(id);
        return new ResponseEntity<Article>(HttpStatus.NO_CONTENT);
    }
    
    //Delete All
    @RequestMapping(method = RequestMethod.DELETE)
    //@DeleteMapping
    public ResponseEntity<Article> deleteAllArticle () {
        List<Article> articles = articleRepository.findAll();
        if (articles.isEmpty()!=true) {
            System.out.println("Articles not found!");
            return new ResponseEntity<Article>(HttpStatus.NOT_FOUND);
        }

        articleRepository.deleteAll();
        return new ResponseEntity<Article>(HttpStatus.NO_CONTENT);
    }
    
    //ReplaceAll
    @RequestMapping(method = RequestMethod.PUT)
    //@PutMapping
    public ResponseEntity<Article> updateArticle(@RequestBody List<Article> articles) {
        List<Article> oldArticles = articleRepository.findAll();
        int i=0;
       for(Article c : articles){
           if(i<oldArticles.size()) {
               c.setId(oldArticles.get(i).getId());
               i++;
           }
           articleRepository.save(c);
        }
        return new ResponseEntity<Article>(HttpStatus.NO_CONTENT);
    }

    //Replace By Id
    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    //@PutMapping("/{id}")
    public ResponseEntity<Article> updateArticle(@RequestBody Article article, @PathVariable("id") long id) {
        article.setId(id);
        articleRepository.save(article);
        return new ResponseEntity<Article>(HttpStatus.NO_CONTENT);
    }

    //Update By Id
    @RequestMapping(value="/{id}", method = RequestMethod.PATCH)
    //@PatchMapping("/{id}")
    public ResponseEntity<Article> updatePartOfArticle(@RequestBody Map<String, Object> updates, @PathVariable("id") long id) {
        Article article = articleRepository.findById(id);
        if (article == null) {
            System.out.println("Article not found!");
            return new ResponseEntity<Article>(HttpStatus.NOT_FOUND);
        }
        partialUpdate(article,updates);
        return new ResponseEntity<Article>(HttpStatus.NO_CONTENT);
    }

    //Partial update
    private void partialUpdate(Article article, Map<String, Object> updates) {
        if (updates.containsKey("name")) {
            article.setName((String) updates.get("name"));
        }
        articleRepository.save(article);
    }

}


