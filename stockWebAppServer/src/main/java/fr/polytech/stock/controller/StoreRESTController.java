package fr.polytech.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.polytech.stock.model.Store;
import fr.polytech.stock.repository.StoreRepository;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/restApi/stores")
public class StoreRESTController {

    private StoreRepository storeRepository;

    @Autowired
    public StoreRESTController(StoreRepository storeRepository) {
        this.storeRepository = storeRepository;
    }

    //get All
    @RequestMapping(method = RequestMethod.GET/*, produces = "application/xml"*/)
    //@GetMapping
    public List<Store> findAllStores() {
        return storeRepository.findAll();
    }
    
    //get By Id
    @RequestMapping(value="/{id}", method = RequestMethod.GET/*, produces = "application/xml"*/)
    //@GetMapping
    public Store findStore(@PathVariable("id") long id) {
        return storeRepository.findById(id);
    }
    
    //Create new entry
    @RequestMapping(method = RequestMethod.POST)
    //@PostMapping
    public ResponseEntity<Store> addStore(@RequestBody Store store) {
        storeRepository.save(store);
        return new ResponseEntity<Store>(store, HttpStatus.CREATED);
    }

    //Delete By Id
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    //@DeleteMapping("/{id}")
    public ResponseEntity<Store> deleteStore (@PathVariable("id") long id) {
        Store store = storeRepository.findById(id);
        if (store == null) {
            System.out.println("Store not found!");
            return new ResponseEntity<Store>(HttpStatus.NOT_FOUND);
        }

        storeRepository.deleteById(id);
        return new ResponseEntity<Store>(HttpStatus.NO_CONTENT);
    }
    
    //Delete All
    @RequestMapping(method = RequestMethod.DELETE)
    //@DeleteMapping
    public ResponseEntity<Store> deleteAllStore () {
        List<Store> stores = storeRepository.findAll();
        if (stores.isEmpty()!=true) {
            System.out.println("Stores not found!");
            return new ResponseEntity<Store>(HttpStatus.NOT_FOUND);
        }

        storeRepository.deleteAll();
        return new ResponseEntity<Store>(HttpStatus.NO_CONTENT);
    }
    
    //ReplaceAll
    @RequestMapping(method = RequestMethod.PUT)
    //@PutMapping
    public ResponseEntity<Store> updateStore(@RequestBody List<Store> stores) {
        List<Store> oldStores = storeRepository.findAll();
        int i=0;
       for(Store c : stores){
           if(i<oldStores.size()) {
               c.setId(oldStores.get(i).getId());
               i++;
           }
           storeRepository.save(c);
        }
        return new ResponseEntity<Store>(HttpStatus.NO_CONTENT);
    }

    //Replace By Id
    @RequestMapping(value="/{id}", method = RequestMethod.PUT)
    //@PutMapping("/{id}")
    public ResponseEntity<Store> updateStore(@RequestBody Store store, @PathVariable("id") long id) {
        store.setId(id);
        storeRepository.save(store);
        return new ResponseEntity<Store>(HttpStatus.NO_CONTENT);
    }

    //Update By Id
    @RequestMapping(value="/{id}", method = RequestMethod.PATCH)
    //@PatchMapping("/{id}")
    public ResponseEntity<Store> updatePartOfStore(@RequestBody Map<String, Object> updates, @PathVariable("id") long id) {
        Store store = storeRepository.findById(id);
        if (store == null) {
            System.out.println("Store not found!");
            return new ResponseEntity<Store>(HttpStatus.NOT_FOUND);
        }
        partialUpdate(store,updates);
        return new ResponseEntity<Store>(HttpStatus.NO_CONTENT);
    }

    //Partial update
    private void partialUpdate(Store store, Map<String, Object> updates) {
        if (updates.containsKey("name")) {
            store.setName((String) updates.get("name"));
        }
        storeRepository.save(store);
    }

}


