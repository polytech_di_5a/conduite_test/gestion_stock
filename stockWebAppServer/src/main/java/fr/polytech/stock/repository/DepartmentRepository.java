package fr.polytech.stock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.polytech.stock.model.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department,Long> {
    Department findById(long id);
}