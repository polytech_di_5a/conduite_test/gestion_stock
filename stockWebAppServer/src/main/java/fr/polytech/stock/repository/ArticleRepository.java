package fr.polytech.stock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.polytech.stock.model.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article,Long> {
	Article findById(long id);
}


