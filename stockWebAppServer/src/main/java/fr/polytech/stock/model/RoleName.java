package fr.polytech.stock.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
