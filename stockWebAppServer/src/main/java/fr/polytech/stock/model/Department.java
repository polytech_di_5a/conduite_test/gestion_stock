package fr.polytech.stock.model;

import javax.persistence.*;

@Entity
public class Department {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne(fetch = FetchType.EAGER)
    private User user;
    private String name;
    @ManyToOne( cascade = {CascadeType.MERGE} )
    private Store store;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
}
