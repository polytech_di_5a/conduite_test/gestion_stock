package fr.polytech.stock.model;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import java.util.Set;

import static org.junit.Assert.*;

public class UserTest {

    private User user;
    private Validator validator;

    @Test
    public void initUser(){

        validator = Validation.buildDefaultValidatorFactory().getValidator();

        user = new User("test","password");

        Set<ConstraintViolation<User>> violations = validator.validate(user);
        assertEquals(0, violations.size() );


        user = new User("te","password");

        violations = validator.validate(user);
        assertEquals(true, violations.size() > 0 );


        user = new User("test","pa");

        violations = validator.validate(user);
        assertEquals(true, violations.size() > 0 );


        user = new User("","password");

        violations = validator.validate(user);


        assertEquals(true, violations.size() > 0 );


        user = new User("test","");

        violations = validator.validate(user);
        assertEquals(true, violations.size() > 0 );

        String testString = "test";
        for(int i = 0; i < 50; i++ )
            testString += "t";

        user = new User(testString,"password");

        violations = validator.validate(user);
        assertEquals(true, violations.size() > 0 );


        String passString = "password";
        for(int i = 0; i < 100; i++ )
            passString += "d";

        user = new User("test",
                passString);

        violations = validator.validate(user);
        assertEquals(true, violations.size() > 0 );


    }



}
