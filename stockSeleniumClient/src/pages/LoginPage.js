'use strict';

class LoginPage {
  
  constructor() {
    this.userNameField = element(by.name('username'));
    this.passwordField = element(by.name('password'));
    this.loginButton = element(by.buttonText('Login'));
    this.content = element(by.tagName('body'));
  }

  async navigateToLogin(pageUrl) {
    await browser.get(pageUrl);
    return browser.wait(EC.visibilityOf(this.loginButton), 20000);
  }

  async loginUser(userName, password) {
    await this.userNameField.sendKeys(userName);
    await this.passwordField.sendKeys(password);
    return this.loginButton.click();
  }
}

module.exports = new LoginPage();