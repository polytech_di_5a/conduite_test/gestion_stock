'use strict';

class ArticlePage {
  
  constructor() {
    this.addButton = element(by.buttonText('add'));
  }

  async navigateToArticle(pageUrl) {
    await browser.get(pageUrl);
    return browser.wait(EC.visibilityOf(this.addButton), 20000);
  }
}

module.exports = new ArticlePage();