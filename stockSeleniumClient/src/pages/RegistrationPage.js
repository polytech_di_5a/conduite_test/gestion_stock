'use strict';

class RegistrationPage {

  constructor() {
    this.userNameField = element(by.name('username'));
    this.passwordField = element(by.name('password'));
    this.birthdayField = element(by.name('birthday'));
    this.registerButton = element(by.buttonText('Register'));
    this.loginButton = element(by.buttonText('Login'));
  }

  async navigateToRegistration(pageUrl) {
    await browser.get(pageUrl);
    return browser.wait(EC.visibilityOf(this.registerButton), 20000);
  }

  async registerUser(userData) {
    await this.userNameField.sendKeys(userData.userName);
    await this.passwordField.sendKeys(userData.password);
    await this.birthdayField.sendKeys(userData.birthday);
    return this.registerButton.click();
    //return element(by.buttonText('Login')).click();
  }
}

module.exports = new RegistrationPage();