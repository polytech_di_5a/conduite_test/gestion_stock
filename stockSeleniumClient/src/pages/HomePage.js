'use strict';

class HomePage {
  
  constructor() {
    this.logoutButton = element(by.buttonText('Logout'));
    this.content = element(by.tagName('app-home'));
  }

  async navigateToLogout(pageUrl) {
    await browser.get(pageUrl);
    return browser.wait(EC.visibilityOf(this.logoutButton), 20000);
  }

  async logout() {
    return this.logoutButton.click();
  }
}

module.exports = new HomePage();