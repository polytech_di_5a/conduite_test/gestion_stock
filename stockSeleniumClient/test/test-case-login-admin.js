'use strict';
const params = browser.params;
const RegistrationPage = require('../src/pages/RegistrationPage');
const LoginPage = require('../src/pages/LoginPage');
const ArticlePage = require('../src/pages/ArticlePage');
const HomePage = require('../src/pages/HomePage');
let newUser;

describe('Login app with redirect tests', () => {
    beforeAll(async() => {
      newUser = {
        userName: "superadmin",
        password: "verygoodpassword"
      };
      return LoginPage.navigateToLogin(params.loginPageUrl);
    });
    it('Should login user into system', async() => {
      await LoginPage.loginUser(newUser.userName, newUser.password);
      browser.sleep(2000);
      expect(LoginPage.content.isDisplayed()).toBe(true);
      browser.sleep(2000);
      expect(LoginPage.content.getText()).toContain(newUser.userName);
    });
    it('Should move to article page', async() => {
      await ArticlePage.navigateToArticle(params.articlePageUrl);
      browser.sleep(2000);
    });
    it('Should move to logout page and logoff', async() => {
      await HomePage.navigateToLogout(params.homePageUrl);
      await HomePage.logout();
      browser.sleep(2000);
      expect(HomePage.content.isDisplayed()).toBe(true);
      browser.sleep(2000);
      expect(HomePage.content.getText()).toContain("Please login");
    });
});