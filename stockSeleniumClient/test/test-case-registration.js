'use strict';
const params = browser.params;
const RegistrationPage = require('../src/pages/RegistrationPage');
const LoginPage = require('../src/pages/LoginPage');
let newUser;

describe('Login app with redirect tests', () => {
    beforeAll(async() => {
      newUser = {
        userName: faker.internet.userName(),
        password: faker.internet.password(),
        birthday: "24/02/1998"
      };
      return RegistrationPage.navigateToRegistration(params.registrationPageUrl);
    });
    it('Should register user in system', async() => {
      await RegistrationPage.registerUser(newUser);
      expect(RegistrationPage.loginButton.isDisplayed()).toBe(true);
    });
    it('Should login user into system', async() => {
      LoginPage.navigateToLogin(params.loginPageUrl);
      await LoginPage.loginUser(newUser.userName, newUser.password);
      browser.sleep(2000);
      expect(LoginPage.content.isDisplayed()).toBe(true);
      browser.sleep(2000);
      expect(LoginPage.content.getText()).toContain(newUser.userName);
    });
});