// spec.js
describe('Protractor Demo App', function() {
  it('should have a title', function() {
    browser.get('http://localhost:4200/home');

    expect(browser.getTitle()).toEqual('Stock Web App');
  });
});