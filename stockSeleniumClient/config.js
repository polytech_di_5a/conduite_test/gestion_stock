'use strict';

exports.config = {
  directConnect: true,
  framework: 'jasmine',
  capabilities: {
    browserName: 'chrome'
  },
  params: {
    registrationPageUrl: 'http://localhost:4200/signup',
    loginPageUrl: 'http://localhost:4200/auth/login',
    articlePageUrl: 'http://localhost:4200/admin/articles',
    homePageUrl: 'http://localhost:4200/home'
  },
  specs: [
    'test/test-connexion.js',
    'test/test-case-login-admin.js',
    'test/test-case-registration.js'
  ],
  onPrepare: function() {
    global.faker = require('faker');
    global.EC = protractor.ExpectedConditions;
  }
}
