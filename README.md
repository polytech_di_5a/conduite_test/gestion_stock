# StockWebApp

## Lancement de l'application

Etapes :
1. Lancer un server postgres
2. Créer une base nommée "stock"
2. Lancer la partie server
3. Lancer la partie client
4. optionnel : lancer la partie test selenium

### Linux

Lancement du server postgres :
```
sudo systemctl start postgresql.service 
```
Depuis la racine du projet :
1. Lancement du server
```
cd stockWebAppServer/
java -jar target/stockwebapp-0.0.1-SNAPSHOT.war
```
2. Depuis un autre terminal, lancement du client
```
cd stockWebAppClient/
ng serve
```
3. (Optionnel) Lancement des tests Sélénium (Depuis un autre terminal
```
cd stockSeleniumClient/
protractor config.js
```

## Structure

3 Projets :
- Partie serveur (spring boot) + test Junit & postman
- Partie client (angular)
- Test selenium (pour la partie client)

## Dependence server:

L'application nécessite une base de données Postgresql.
adresse : localhost
- port : 5432 (port par defaut de postgres)
- nom de base : stock
- user : admin
- password : password

Ces données peuvent être modifiées en mettant à jour le fichier : 
ressource/application.proporties


## Comptes de bases :
Super utilisateur (pdg) :
- login : superadmin
- pwd : verygoodpassord

